import java.util.Scanner;
public class AccountDetails {
	public static Account getAccountDetails()
	{
	Scanner sc=new Scanner(System.in);
	Account a=new Account();
	System.out.println("Enter account id:");
	int id=sc.nextInt();
	a.setAccountId(id);
	System.out.println("Enter account type:");
	String type=sc.next();
	a.setAccountType(type);
	int n=1,balance=0;
	while(n>0)
	{
	System.out.println("Enter balance:");
	balance=sc.nextInt();
	if(balance<=0)
	{
		System.out.println("Balance should be positive");
	}
	else
	{
		break;
	}
	}
	a.setBalance(balance);
	return a;	
	}
	public static int getWithdrawAmount()
	{
		Scanner sc=new Scanner(System.in);	
		int n=1,amount=0;
		while(n>0)
		{
		System.out.println("Enter amount to be withdrawn:");
		amount=sc.nextInt();
		if(amount<=0)
		{
			System.out.println("Amount should be positive");
		}
		else
		{
			break;
		}
		}
		return amount;
	}
 public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	Account b=getAccountDetails();
	int amnt=0;
	amnt=getWithdrawAmount();
	b.withdraw(amnt);	
}
}
